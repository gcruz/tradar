$(document).on("ready",function(){
	$(".dropdown").on("click",function(){
		$(".mainMenu").slideToggle();
	});
	$(".slick").slick({dots:!0,infinite:!0,speed:300,slidesToShow:1,adaptiveHeight:!0});
	$(".mainMenu > li > a, .menu > li > a").click(function(e) { 
		e.preventDefault();
		goScrollTo($(this).attr("href"));           
	});
});

function goScrollTo(id){
    var id = id.replace("#", "");
    $('html,body').animate({scrollTop: $("."+id).offset().top},'slow');
}